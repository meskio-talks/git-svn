TEX_FILES = git-svn


all: clean pdf

pdf: ${TEX_FILES:%=%.pdf} clean-tmp

%.dvi: %.tex
	-latex $<
	latex $<

%.pdf: %.dvi
	dvipdf $<

clean:
	rm -rf ${TEX_FILES}.pdf

clean-tmp:
	rm -rf *.dvi *.log *.aux
